#!/usr/bin/R
suppressMessages(library("dplyr"))

## ------------------------- Parameters -------------------------
# args[1] = File with extended annotations from ID mapping
# args[2] = Directory with predictions

args = commandArgs(trailingOnly=TRUE)
extFile <- args[1]
prdDir <- args[2]

# ---------------------------------------------------------------
## ---------------------------- Main ----------------------------

# -- Load data

# Extended interactions file

ext.dat<-read.table(extFile,header = T,sep="\t",fill = T,stringsAsFactors = F,quote = "")
ext.dat$pmid<-as.factor(ext.dat$pmid)
ext.dat$gene_id<-as.factor(ext.dat$gene_id)
ext.dat$disease_id<-as.factor(ext.dat$disease_id)
ext.dat$int<-as.factor(paste(ext.dat$pmid,ext.dat$gene_id,ext.dat$disease_id,sep="||"))
# Predictions 

files <- list.files(prdDir,pattern = "txt")

for(file in files){
	
	prdFile<-paste(prdDir,file,sep="/")
	extpreFile<-paste(prdDir,sub("\\.txt","-extended.txt",file),sep="/")
	pre.dat<-read.table(prdFile,header = T,sep="\t",fill = T,stringsAsFactors = F)
	pre.dat$int<-paste(pre.dat$pmid,pre.dat$gene_id,pre.dat$disease_id,sep="||")

	# Merged data set
	out.dat<-merge(pre.dat, ext.dat,by="int",all.x = TRUE)
	rownames(out.dat)<-NULL
	out.dat<-out.dat[,c('pmid.x','gene_id.x', 'disease_id.x', 'pred_class', 'proba','gene_name', 'disease_name', 'sentence')]
	colnames(out.dat)<-c('pmid','gene_id', 'disease_id', 'pred_class', 'proba','gene_name', 'disease_name', 'sentence')
	write.table(out.dat,row.names = F,quote = F,file=extpreFile,sep='\t')
}

