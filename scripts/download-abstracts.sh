# $1 = number of cores to run in parallel 

# Download annotated abstracts

echo "Downloading annotated abstracts from PubTator..."

# Calculate lines per file 

cores=$1
nl=$(wc -l pubtator-downloads.sh| grep -o -e '[0-9]*')

echo "Number of downloads: $nl"
echo "Number of cores: $cores"

if [ "$nl" -lt "$cores" ]
then
	lines=$((nl / 2))
	echo "Downloads will run in parallel in 2 cores"
	echo "Downloads per core: $lines"
else
	lines=$((nl / cores))
	echo "Downloads will run in parallel in $cores cores"
	echo "Downloads per core: $lines"
fi

split -l "$lines" pubtator-downloads.sh "$1" --additional-suffix ptdw

for f in $(ls *ptdw*)
do
	chmod 700 "$f"
	./$f &
done

