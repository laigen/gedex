import os

fn = []
indir = 'text-fmt-abs/'
for(_, _, filenames) in os.walk(indir):
	fn.extend(filenames)

fn = [indir+f for f in fn]

num_files = len(fn)
print('There are {} files in text-fmt-abs'.format(num_files))

from_ = 0
to_ = 0
by_ = 99999


newfile = open('iter_cats.sh', 'w')
while to_ != num_files-1:
	to_ = from_ + by_
	
	if to_ > num_files:
		to_ = num_files - 1

	

	newline = ' '.join(['cat'] + fn[from_:to_] + ['>', 'small_cats/small_{}-{}.txt'.format(from_, to_)])

	newfile.write(newline)
	newfile.write('\n')

	from_ = to_ + 1

newfile.close()
