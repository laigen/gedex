import re
from collections import defaultdict as df
from optparse import OptionParser
import os
import sys
from joblib import Parallel, delayed
import multiprocessing

# ---------------------------------------- Parameters ---------------------------------------- 

parser = OptionParser()
parser.add_option("-i", dest="inF",help="Input file", metavar="PATH")
parser.add_option("-o", dest="outF",help="Output file",metavar="PATH")
parser.add_option("-n", dest="nc",help="number of cores", type=int)


(options, args) = parser.parse_args()
if len(args) > 0:
	parser.error("None parameters indicated.")
	sys.exit(1)

inFile=options.inF
outFile=options.outF
num_cores = options.nc


# -------------------------------------------------------------------------------------------- 
# ---------------------------------------- Functions ----------------------------------------- 

def change_anot_format(par):

	pmid = par[0]
	abst = par[1]

	abstracts = {}
	lenPmid = 0
	pos = 0
	pmidRegex = re.compile(r'(\d+)\|t\|')

	for line in abst:

		line = str(line)
		if not re.search('\t',line):
			result = pmidRegex.search(line)
			if result:
				pmid = result.group(1)
				pos=0
				abstracts[pmid] = line
				lenPmid = len(pmid) + 3
			else:
				print("Warning: no PMID")
		else:
			annots = line.split('\t')
			i = int(annots[1])+lenPmid+pos
			j = int(annots[2])+lenPmid+pos
			term = abstracts[pmid][i:j]
			#print("----------------------------------------")
			#print("Pmid: {}".format(pmid))
			#print("term: {}".format(term))
			#print("class: {}".format(annots[4]))
			tag = annots[4].rstrip("\n")
			if tag=='Gene':
				abstracts[pmid] = abstracts[pmid][:i] + "<g>" + abstracts[pmid][i:j] + "</g>" + abstracts[pmid][j:]
				pos+=7
			if tag=='Disease':
				abstracts[pmid] = abstracts[pmid][:i] + "<d>" + abstracts[pmid][i:j] + "</d>" + abstracts[pmid][j:]
				pos+=7
			if tag=='Positive_regulation':
				abstracts[pmid] = abstracts[pmid][:i] + "<v>" + abstracts[pmid][i:j] + "</v>" + abstracts[pmid][j:]
				pos+=7
			if tag=='Gene_expression':
				abstracts[pmid] = abstracts[pmid][:i] + "<e>" + abstracts[pmid][i:j] + "</e>" + abstracts[pmid][j:]
				pos+=7
			if tag=='Localization':
				abstracts[pmid] = abstracts[pmid][:i] + "<l>" + abstracts[pmid][i:j] + "</l>" + abstracts[pmid][j:]
				pos+=7
			if tag=='Regulation':
				abstracts[pmid] = abstracts[pmid][:i] + "<r>" + abstracts[pmid][i:j] + "</r>" + abstracts[pmid][j:]
				pos+=7
			if tag=='Negative_regulation':
				abstracts[pmid] = abstracts[pmid][:i] + "<u>" + abstracts[pmid][i:j] + "</u>" + abstracts[pmid][j:]
				pos+=7
			if tag=='Phosphorylation':
				abstracts[pmid] = abstracts[pmid][:i] + "<p>" + abstracts[pmid][i:j] + "</p>" + abstracts[pmid][j:]
				pos+=7

	return abstracts

def write_abstract(abst):
	pmid = str(list(abst.keys())[0])
	outFile = 'text-fmt-abs/'+pmid+'.txt'
	with open(outFile,'w',encoding="utf-8") as of:
		for key in abst.keys():
			of.write(abst[key])


# -------------------------------------------------------------------------------------------- 
# ------------------------------------------- Main -------------------------------------------

corpusDir = df(list)
print('---> Reading input files')
with open(inFile,'r',encoding='utf-8') as file:
	for line in file:
		pmid = re.search(r'^(\d+)',line).group(1)
		#print("Pmid: {}".format(pmid))
		corpusDir[pmid].append(line)

print('---> Building parameters list')
pars = [(pmid,corpusDir[pmid]) for pmid in corpusDir.keys()]
print('---> Changing annotation format')
allAbstracts = Parallel(n_jobs=num_cores)(delayed(change_anot_format)(par) for par in pars)
print('---> Writing output file')
Parallel(n_jobs=num_cores)(delayed(write_abstract)(abst) for abst in allAbstracts)
print('===> Finished changing annotation format.')





