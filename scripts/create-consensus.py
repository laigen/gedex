import re
import os
import glob
import pandas as pd
from optparse import OptionParser
from joblib import Parallel, delayed

parser = OptionParser()
parser.add_option("-i", dest="indir",help="Prediction directory", metavar="PATH")
parser.add_option("-t", dest="pubT",help="Pubtator synonyms file", metavar="PATH")
parser.add_option("-c", dest="ncore",help="Number of cores", type=int)
parser.add_option("-o", dest="outdir",help="Output directory", metavar="PATH")

(options, args) = parser.parse_args()

if len(args) > 0:
	parser.error("Please indicate all necessary inputs")
	sys.exit(1)

pubtator_syn = options.pubT
input_dir = options.indir
num_cores = options.ncore
out_dir = options.outdir

################################################## Reading files ###########################################
print('---> Reading synonyms from file {}'.format(pubtator_syn))

pubtator_synonym = pd.read_csv(pubtator_syn, sep='\t', encoding='utf-8', quoting=3, dtype={'PMID':str, 'synonyms ID':str, 'Pubtator ID':str})

# Creating dataframe for joining
synonym_relation = pubtator_synonym.loc[pubtator_synonym['type']=='Gene', ['PMID', 'Pubtator ID', 'synonyms ID']].drop_duplicates()
synonym_relation.rename(columns={'PMID':'pmid', 'synonyms ID':'gene_id'}, inplace=True)
synonym_relation.set_index(['pmid', 'gene_id'], inplace=True)

prediction_files = glob.glob(os.path.join(input_dir, 'predictions-*extended.txt'))

############################################### Consensus function #####################################

def generate_outputline(data):
    data['pmid-sentence'] = data['pmid'] + ':' + data['sentence']
    gene_id = data['gene_id'].drop_duplicates().values[0]
    disease_id = data['disease_id'].drop_duplicates().values[0]
    interaction = '|'.join([gene_id, disease_id])
    class_ = data['pred_class'].drop_duplicates().values[0]
    proba = data['proba'].drop_duplicates().values[0]

    ev_sents = data['pmid-sentence'].drop_duplicates().tolist()
    evidence_sentences = '|'.join([str(s) for s in ev_sents])

    pub_ID = data['Pubtator ID'].drop_duplicates().tolist()
    ncbi_str = '|'.join([str(p) for p in pub_ID])
    #print(data['Pubtator ID'])

    gnames = data['gene_name'].drop_duplicates().tolist()
    gene_name_str = '|'.join(str(g) for g in gnames)

    dnames = data['disease_name'].drop_duplicates().tolist()
    disease_name_str = '|'.join([str(d) for d in dnames])

    pmids = data['pmid'].drop_duplicates().tolist()
    pmid_str = '|'.join(str(p) for p in pmids)
    
    out = [interaction, class_, proba, gene_id, disease_id, ncbi_str, gene_name_str, disease_name_str, pmid_str, evidence_sentences]
    return(out)

 ######################################### Converting prediction to consensus #############################
print('---> Converting prediction to consensus...')

for file in prediction_files:
	filename = file.split('/')[-1]
	outfile = out_dir + "/" + re.sub('\.txt', '-consensus.txt', filename)

	print('::: Converting {} '.format(filename))

	prediction_df = pd.read_csv(file, sep='\t', quoting=3, encoding='utf-8', dtype={'pmid':str,'gene_id':str,'disease_id':str})
	prediction_df.set_index(['pmid', 'gene_id'], inplace=True)

	predictions_org = prediction_df.join(synonym_relation, on=prediction_df.index.name)
	predictions_org.reset_index(inplace=True)
	

	results = Parallel(n_jobs=num_cores)(delayed(generate_outputline)(df) for index, df in predictions_org.groupby(['gene_id', 'disease_id']))

	output_df = pd.DataFrame(results, columns=['interaction', 'Predicted-class', 'Probability', 'Internal-gene-ID', 'Disease-ID', 'NCBI-gene-IDs', 'Gene-names', 'Disease-names', 'PMID', 'Evidence-sentences'])
	output_df.to_csv(outfile, sep='\t', encoding='utf-8', quoting=3, index=False)
print('Finished final output.')
