from sklearn.externals import joblib
import pandas as pd
import numpy as np
from optparse import OptionParser
import os
import sys
import re

# --------------------------- Parameters -----------------------------------

# -i : file containing the testing interactions that will be used  
# -m : directory containing trained models
# -f : Directory to save the confusion matrices and plots 

#--------------------------------------------------------------------------

parser = OptionParser()
parser.add_option("-i", dest="iF",help="Directory with evaluation data sets", metavar="PATH")
parser.add_option("-m", dest="mF",help="Directory containing trained model",metavar="PATH")
parser.add_option("-o", dest="oF",help="Directory to store prediction outputs",metavar="PATH")

(options, args) = parser.parse_args()
if len(args) > 0:
    parser.error("None parameters indicated.")
    sys.exit(1)

inFile = options.iF
modDir = options.mF
outDir = options.oF

#-------------------------------- Main ---------------------------------------

#Load data 

models = [f for f in os.listdir(modDir) if re.match(r'(.*)mod', f)]
data = pd.read_csv(inFile,sep='\t',encoding='utf-8')

data['pmid'] = data['pmid'].astype(str)

for model in models:

	mod = modDir + '/' + model
	clf = joblib.load(mod)

	#Get features

	feats = ['gcf', 'dcf', 'icf', 'cps']
	features = data[feats]

	#Classify

	y_pred = clf.predict(features)
	y_prob = clf.predict_proba(features)[:,1]

	#Build output

	dataOut = data.drop(feats,axis=1,inplace=False)
	dataOut['pred_class'] = y_pred
	dataOut['proba'] = y_prob

	#Save output 
	modname = re.sub(r'\.mod','',model)
	outFile = outDir+"/predictions-"+modname+".txt" 
	dataOut.to_csv(path_or_buf = outFile,sep='\t',index=False,encoding='utf-8')




















