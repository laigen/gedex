#===============================================================================================================
#==================================================== GeDEx ====================================================

# This is the GeDEx pipeline for the automatic extraction of interactions between genes and diseases from 
# biomedical literature
# Authors: Roberto Olayo Alarcon and Larisa Morales Soto
# Institution: Prorgam of computational genomics, UNAM

# --------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------

#===============================================================================================================
# --------------------------------------------------- HELP -----------------------------------------------------

parameters:
	@echo ""
	@echo "  - Libraries:"
	@echo ""
	@echo "  * TEESDir			Path to TEES directory"
	@echo "  * iSimpDir			Path to iSimp directory"
	@echo "  * coreDir			Path to Stanford CoreNLP directory"
	@echo ""
	@echo "  - Download and annotation files:"
	@echo ""
	@echo "  * corpusName			Name that will be given to the corpus"
	@echo "  * pmidFile			File specifiyng the PubMed id's (one per line)"
	@echo "  * corpusDir			Directory to store corpus files (in th form corpusName-corpora)"
	@echo "  * miRNADic			Path to json micro RNA genes dictinoary"
	@echo "  * PTcorpusPath		Path to annotated formatted abstracts from PubTator"
	@echo "  * PTcorpusPathOriginal	Path to store PubTator raw output"
	@echo "  * TTcorpusPath		Path to annotated abstracts with HTML tags within text"
	@echo "  * NAcorpusPath		Path to not annotated abstracts"
	@echo ""

#===============================================================================================================
# ----------------------------------------------- PARAMETERS ---------------------------------------------------

# User modifiable 

corpusName = input-corpora
coreNum = 20

# Path to external libraries and dictionaries 

TEESDir = ../lib/TEES-2.2.1
iSimpDir = ../lib/format
coreDir = ../lib/stanford-corenlp-full-2018-02-27
ctdF = ../reference/CTD_diseases/CTD_diseases.tsv
greekJSON = ../dictionaries/greek_eng_dict.json

# Download and annotation 

corpusDir = ../${corpusName}
pmidFile = ${corpusDir}/pmids.txt
miRNADic = ../dictionaries/miRNA.json
PTcorpusPath = ${corpusDir}/tab-annotated-abstracts.txt
PTcorpusPathOriginal = ${corpusDir}/original-tab-annotated-abstracts.txt
TTcorpusPath = ${corpusDir}/text-annotated-abstracts.txt
NAcorpusPath = ${corpusDir}/not-annotated-abstracts.txt
EntityEffectPath = ${corpusDir}/entity-effect-tees.txt

# Sentence splitting and simplification 

SScorpusPath = ${corpusDir}/simplified-annotated-sentences.txt
SSlibPath = ${corpusDir}/lib/sentence-simplification/algorithm_sentences

# Sentence filtering 

ALLcorpusPath = ${corpusDir}/all-sentences

#Interactions 

interPath = ${corpusDir}/interactions
interPathmmx=${interPath}/mmx/full

#Classification

modPath = ../models
predPath = ${corpusDir}/predictions

#===============================================================================================================
# ------------------------------------------------ TARGETS -----------------------------------------------------

download-pubtator:
	@echo ""
	@echo "================================= Downloads =================================="
	@echo ""
	@mkdir -p ${corpusDir}/all-abstract-files
	@mkdir -p ${corpusDir} 
	@mkdir -p ${corpusDir}/raw-abstracts-pubtator
	@touch ${PTcorpusPathOriginal}
	@echo "---> Building PubTator instructions to download abstracts from ${pmidFile} ..."
	@./create-dwld-request.sh ${pmidFile} ${corpusDir}/all-abstract-files
	@echo "---> Finished building download instructions"
	@./download-abstracts.sh ${coreNum}
	@echo ""

format-pubtator:
	@echo "------------------------------------------------------------------------------"
	@echo "==> Finished downloading abstracts from PubTator."
	@echo "------------------------------------------------------------------------------"
	@echo ""
	@echo "=================================== Format ===================================="
	@echo ""
	@echo "Formatting downloaded abstracts ..."
	@python3 abstract-formatting-json-v2.py -i ${corpusDir}/all-abstract-files -c ${coreNum}
	@echo ""
	@echo "------------------------------------------------------------------------------"
	@echo "==> Finished formatting abstracts."
	@echo "------------------------------------------------------------------------------"

complete-downloads:
	@echo ""
	@echo "================================= Downloads =================================="
	@echo ""
	@echo "---> Finishing incomplete downloads ..."
	@mkdir -p incomplete-abstracts
	@./create-dwld-request.sh ./pubtator.log incomplete-abstracts
	@./download-abstracts.sh ${coreNum}
	@mv incomplete-abstracts/*abs ${corpusDir}/all-abstract-files

retrieve-cleaned-raw-abstracts:
	@cat ${corpusDir}/all-abstract-files/*.abs > ${PTcorpusPathOriginal}
	@cat ${corpusDir}/all-abstract-files/*.fmt > ${PTcorpusPath}
	@echo ""
	@echo "================================ Raw abstracts ================================"
	@echo ""
	@echo "---> Extracting abstracts without annotations ..."
	@./extract-no-annotated-abstracts.sh ${PTcorpusPath} > ${NAcorpusPath}
	@echo "---> Cleaning abstracts ..."
	@python3 ${iSimpDir}/regex.py ${NAcorpusPath} ${corpusDir}/cleaned-file.txt 
	@mv ${corpusDir}/cleaned-file.san ${NAcorpusPath}
	@sed -i 's/(/( /g' ${NAcorpusPath} 
	@sed -i 's/)/ )/g' ${NAcorpusPath} 
	@echo ""
	@echo "------------------------------------------------------------------------------"
	@echo "==> Finished cleaning abstracts."
	@echo "------------------------------------------------------------------------------"

change-annotation-format:
	@echo ""
	@echo "================================ Annotations =================================="
	@echo ""
	@echo "---> Converting annotations to a tag-based format ..."
	@mkdir -p text-fmt-abs small_cats
	@python3 annotation-format.py -i ${PTcorpusPath} -n ${coreNum}
	@python3 create_cat.py
	@chmod 700 iter_cats.sh
	@echo "---> Joining individual files"
	@./iter_cats.sh 
	@cat small_cats/* > ${TTcorpusPath}
	@rm -rf small_cats text-fmt-abs
	@echo ""
	@echo "------------------------------------------------------------------------------"
	@echo "==> Finished changing annotation format."
	@echo "------------------------------------------------------------------------------"

split-sentences:
	@mkdir -p ${corpusDir}/not-annotated-sentences ${corpusDir}/annotated-sentences
	@mkdir -p ${corpusDir}/coreNLP-output ${ALLcorpusPath} 
	@mkdir -p text-fmt-abs small_cats
	@echo ""
	@echo "=================================== Split ===================================="
	@echo ""
	@echo "---> Tokenizing and splitting sentences with stanfordCoreNLP ..."

	# Stanford CoreNLP
	@sed -i -e 's/.$$/ .\n/' ${TTcorpusPath} 
	java -cp "${coreDir}/*" -Xmx100g edu.stanford.nlp.pipeline.StanfordCoreNLP -file ${TTcorpusPath} -annotators tokenize,ssplit -outputFormat  text -outputDirectory ${corpusDir}
	
	# Join output into a single file
	@echo "---> Building sentence files ..."
	@python3 abstract-lines.py -i ${TTcorpusPath}.out -o text-fmt-abs/
	@echo "---> Joining individual files ..."
	@python3 create_cat.py
	@chmod 700 iter_cats.sh
	@./iter_cats.sh 
	@cat small_cats/* > ${TTcorpusPath} 
	@cp ${TTcorpusPath} ${ALLcorpusPath}/orgn-annotated-sentences.txt
	@echo "---> Finished joining files"

	# Move files 

	@mv ${TTcorpusPath}.out ${corpusDir}/coreNLP-output/
	@mv text-fmt-abs ${corpusDir}/annotated-sentences
	@echo ""
	@echo "------------------------------------------------------------------------------"
	@echo "==> Finished splitting sentences."
	@echo "------------------------------------------------------------------------------"

retrieve-interactions:
	@mkdir -p ${interPath}
	@echo ""
	@echo "================================= Interactions ================================"
	@echo "---> Extracting all interactions ..."
	@python3 get-all-interactions.py -i ${TTcorpusPath} -o tmp1.txt
	@sort -u tmp1.txt | uniq | tac > ${interPath}/interactions.txt
	@echo ""
	@echo "------------------------------------------------------------------------------"
	@echo "==> Finished retrieving associations."
	@echo "------------------------------------------------------------------------------"

convert-to-ids:
	@echo ""
	@echo "================================= Normalization ================================"	
	@mkdir -p ${interPath}/inter-mapping ${interPath}/indiv-conversions
	@echo "---> Converting text interactions to identifiers ..."
	@python3 orign_id_mapping.py -t ${PTcorpusPath} -p ${interPath}/interactions.txt --op ${interPath}/interactions.ids -c ${coreNum} -i ${interPath}/inter-mapping
	@echo ""
	@echo "------------------------------------------------------------------------------"
	@echo "==> Finished normalizing associations."
	@echo "------------------------------------------------------------------------------"

compute-scaled-freqs:
	@echo ""
	@echo "================================== Frequencies ================================="	
	@echo "---> Computing scaled corpus frequencies ..."
	@python3 comp-scaled-freqs.py -i ${interPath}/interactions.ids -o frq.tmp -n ${coreNum}
	@sort -u frq.tmp | uniq | tac > ${interPath}/interactions.frq
	@echo ""
	@echo "------------------------------------------------------------------------------"
	@echo "==> Finished calculating frequencies."
	@echo "------------------------------------------------------------------------------"

classify:
	@echo ""
	@echo "================================== Predictions ================================="	
	@echo "---> Classifying interactions ..."
	@mkdir -p ${predPath}
	@python3 classify.py -i ${interPath}/interactions.frq -m ${modPath} -o ${predPath}
	@echo ""
	@echo "------------------------------------------------------------------------------"
	@echo "==> Finished classifying interactions."
	@echo "------------------------------------------------------------------------------"

revert-ids:
	@echo ""
	@echo "================================= Final Output ================================="
	@echo "---> Building final output ..."
	@mkdir -p ${predPath}/original-ids
	@mkdir -p ${predPath}/consensus
	@echo "---> Getting prection's metadata..."
	@Rscript extend-preds.r ${interPath}/interactions.ids ${predPath}
	@echo "---> Reverting to original ids..."
	@python3 revert_original_id.py -i ${predPath} -t ${interPath}/inter-mapping/pubtator_df_syn.txt
	@echo "---> Building consensus format for predictions..."
	@python3 create-consensus.py -i ${predPath}/original-ids -t ${interPath}/inter-mapping/pubtator_df_syn.txt -c ${coreNum} -o ${predPath}/consensus
	@echo ""
	@echo "------------------------------------------------------------------------------"
	@echo " ==> Final output is ready. All predictions are at ${predPath}"
	@echo "------------------------------------------------------------------------------"
	@echo ""
	@echo ""
	@echo "=============================================================================="
	@echo "++++++++++++++++++++++++++++++++ GeDex is done! ++++++++++++++++++++++++++++++"
	@echo "=============================================================================="
	@echo ""
	@echo ""

clean-dir:
	@rm -rf incomplete-abstracts
	@rm -rf *ptdw*
	@rm -f *log
	@rm -f tmp* *tmp
	@rm -f frq.tmp	
	@rm -rf text-fmt-abs
	@rm -rf small_cats
	@rm -rf text-fmt-abs








