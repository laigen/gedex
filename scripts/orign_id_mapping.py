#from __future__ import unicode_literals, print_function
import re
import collections as cl
import pandas as pd
import json
from joblib import Parallel, delayed
from optparse import OptionParser
import os
import json
os.sched_setaffinity(0,list(range(os.cpu_count())))

######################################################################## Reading arguments ##############################################################
parser = OptionParser()
parser.add_option("-t", dest="pubT",help="Original tab annotated abstracts", metavar="PATH")
parser.add_option("-p", dest='pred', help='Prediction file', metavar="PATH")
parser.add_option("--op", dest="outp", help='prediction output file', metavar="PATH")
parser.add_option('-c', dest='ncore', help='Number of cores', type=int)
parser.add_option('-i', dest='inter', help='Intermediary files will be stored here', metavar='PATH')
#parser.add_option('-g', dest='greek', help='Greek to english dictionary', metavar='PATH')
(options, args) = parser.parse_args()

if len(args) > 0:
	parser.error("Please indicate all necessary inputs")
	sys.exit(1)

num_cores = options.ncore
pubtator = options.pubT
prediction = options.pred
outfile = options.outp
inter_dir = options.inter
#greek_json = options.greek


##################################################################### Reading Pubtator File ################################################################


Name_ID = cl.defaultdict(list)
pub_pmids = []
gene_syn = cl.defaultdict(list)
id_syn = cl.defaultdict(list)
semicolon_dict = cl.defaultdict(list)
pattern = re.compile("['\W_-]+")

with open(pubtator, encoding='utf-8', errors='replace') as info:
	for line in info:
		line = line.rstrip()
		#print(line)
		if re.search('\d+\|t\|', line):
			pmid = re.search('(\d+)\|t\|', line).group(1)
			
			if pmid not in pub_pmids:
				pub_pmids.append(pmid)
			continue
		#print(pmid)
		if line.startswith(pmid+'\t'):
		#if line.startswith(pmid + '\t'):
			name_id = re.search(pmid + '\t' + '\d+\t\d+\t(.*)', line).group(1).split('\t')    
			# miR is also classified as Gene 
			if name_id[1] in ['Disease', 'Gene'] and len(name_id) == 3:
				
				if re.search(';',name_id[2]) and name_id[1] == 'Gene' and name_id[0].upper() not in semicolon_dict[name_id[2]]:
					semicolon_dict[name_id[2]].append(name_id[0].upper())

				if name_id[1] == 'Gene' and name_id[0].upper() not in gene_syn[name_id[2]]:
					gene_syn[name_id[2]].append(name_id[0].upper())

				if name_id[1] == 'Disease':
					if name_id[2] == 'None':
						continue
						
					name_id[2] = re.search('\w+?:(.*)', name_id[2]).group(1)
					#print(name_id[2])


				Name_ID['PMID'].append(pmid) 
				Name_ID['name'].append(name_id[0].upper())
				Name_ID['type'].append(name_id[1])
				Name_ID['Pubtator ID'].append(name_id[2])

pubtator_df = pd.DataFrame(Name_ID).drop_duplicates()

########## Reading predictions
print('---> Reading input...')
prediction_df = pd.read_csv(prediction, sep='\t', dtype={'pmid':str, 'snum':str, 'disease':str, 'gene':str}, quoting=3,encoding='utf-8') #csv.QUOTE_NONE
pred_pmids = prediction_df['pmid'].drop_duplicates().values

########################################################## Finding name synonyms and semicolons ######################################################

k = 1
synonym_ids = {}
#### Semicolon
print('---> Looking for semicolon synonyms...')
for key_1 in semicolon_dict.keys():
	for key_2 in semicolon_dict.keys():
		if key_1 == key_2:
			continue
		
		if any([cor for cor in key_1.split(';') if cor in key_2.split(';')]):
			
			if key_1 in synonym_ids.keys():
				synonym_ids[key_2] = synonym_ids[key_1]
			elif key_2 in synonym_ids.keys():
				synonym_ids[key_1] = synonym_ids[key_2]
			else:
				synonym_ids[key_1] = 'a'*k
				synonym_ids[key_2] = 'a'*k
				k += 1
#### Name synonyms
print('---> Looking for named synonyms...')
for gid_1 in gene_syn.keys():
	if re.match('.*?;.*?', gid_1):
		continue
	for gid_2 in gene_syn.keys():
		if gid_1 == gid_2 or re.match('.*?;.*?', gid_2):
			continue
		
		syn_names = [name for name in gene_syn[gid_2] if pattern.sub("", name) in [pattern.sub('', gn) for gn in gene_syn[gid_1]]]
		if any(syn_names):
			if gid_1 in synonym_ids.keys():
				synonym_ids[gid_2] = synonym_ids[gid_1]
			elif gid_2 in synonym_ids.keys():
				synonym_ids[gid_1] = synonym_ids[gid_2]
			else:
				synonym_ids[gid_1] = 'a'*k
				synonym_ids[gid_2] = 'a'*k
				k += 1

##################################################################### Assigning synonyms ##############################################
print('---> Mapping to data frame...')
def assign_syn(row):
	if row['type'] == 'Disease':
		return(row['Pubtator ID'])
	
	
	if row['Pubtator ID'] in synonym_ids.keys():
		return(synonym_ids[row['Pubtator ID']])

	return(row['Pubtator ID'])

pubtator_df['synonyms ID'] = pubtator_df.apply(assign_syn, axis=1)

#### Writing file
out_df = os.path.join(inter_dir, 'pubtator_df_syn.txt')
print('---> Writing pubtator dataframe with synonyms at {}'.format(out_df))

pubtator_df.to_csv(out_df, sep='\t', index=False,encoding='utf-8', quoting=3)


################################################################ Converting prediction function ######################################################
print('---> Converting predictions...')

def convert_prediction(pmid):
	pred_info = prediction_df.loc[prediction_df['pmid']==pmid].copy()
	conversions = []
	pubt_info = pubtator_df.loc[pubtator_df['PMID']==pmid].copy()

	pubt_info['name_4m'] = pubt_info['name'].apply(lambda x: pattern.sub('', x))
	for row in pred_info.itertuples():
		sentence = getattr(row, 'sentence')
		
		gene_name = getattr(row, 'gene')
		gene_name_4m = pattern.sub('', gene_name)
		gene_name_4m = gene_name_4m.upper()

		dise_name = getattr(row, 'disease')
		dise_name_4m = pattern.sub('', dise_name)
		dise_name_4m = dise_name_4m.upper()

		if gene_name_4m in pubt_info.loc[pubt_info['type']=='Gene']['name_4m'].values and dise_name_4m in pubt_info.loc[pubt_info['type']=='Disease']['name_4m'].values:
			pred_gid = pubt_info.loc[(pubt_info['PMID']==pmid) & (pubt_info['type']=='Gene') & (pubt_info['name_4m']==gene_name_4m), 'synonyms ID'].values[0]
			pred_did = pubt_info.loc[(pubt_info['PMID']==pmid) & (pubt_info['type']=='Disease') & (pubt_info['name_4m']==dise_name_4m), 'synonyms ID'].values[0]
			line = '\t'.join([pmid, pred_gid, pred_did, gene_name, dise_name, sentence]) + '\n'
			conversions.append(line)
		else:
			print('::: NotFound - {}, {}, {}'.format(pmid, gene_name_4m, dise_name_4m))
			print('\t'.join(pubt_info.loc[(pubt_info['PMID']==pmid) & (pubt_info['type']=='Gene'), 'name_4m'].drop_duplicates().values))
			print('\t'.join(pubt_info.loc[(pubt_info['PMID']==pmid) & (pubt_info['type']=='Disease'), 'name_4m'].drop_duplicates().values))
			
	return(''.join(conversions))

############################################################  Conversion ##################################
results = Parallel(n_jobs=num_cores, backend="threading")(delayed(convert_prediction)(p) for p in pred_pmids)
print('---> Writing output...')
newline = ''.join(results)
newfile = open(outfile, 'w',encoding='utf-8', errors='replace')
headline = '\t'.join(['pmid','gene_id', 'disease_id', 'gene_name', 'disease_name', 'sentence']) + '\n'
newfile.write(headline)
newfile.write(newline)
newfile.close()
