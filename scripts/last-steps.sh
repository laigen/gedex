# Convert to ids

make convert-to-ids

# Compute freqs

make compute-scaled-freqs

# Classify interactions

make classify

# Generate final output with original IDs

make revert-ids

# Clean temporary files

make clean-dir
