## ------------ annotated-abstracts-download-pipeline --------------#
## DESCRIPTION: This shell script recieves a file containing a list 
# of pubmed ids (one per line) and builds a .sh file containing the 
## instructions to download each annotated abstract from PubTator 
## and appends it to the file tab-annotated-abstracts.txt .
## Then the annotaded abstracts are formated whit a python script to 
## change the annotaitons from a tab-delimited file to tags within 
## the text.
## USAGE:  ./pubTator-abstracts-download.sh ../
## ----------------------------------------------------------------#

# Generate URLs for each pmid
# $1 = pmid file 

touch pubtator-downloads.sh

while read p; 
do
	echo "wget -q -nv https://www.ncbi.nlm.nih.gov/research/pubtator-api/publications/export/biocjson?pmids=$p -O $2/$p.abs | sleep 2.5" >> pubtator-downloads.sh
	#echo "wget -q -nv https://www.ncbi.nlm.nih.gov/CBBresearch/Lu/Demo/RESTful/tmTool.cgi/BioConcept/$p/JSON/ -O $2/$p.abs | sleep 2.5" >> pubtator-downloads.sh
done<"$1"


