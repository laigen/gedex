import matplotlib
matplotlib.use('Agg')
from sklearn.externals import joblib
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import itertools
import numpy as np
from sklearn.metrics import classification_report
import re
import pandas as pd
from optparse import OptionParser
import os
import sys


# --------------------------- Parameters -----------------------------------
# --------------------------------------------------------------------------

# -i : file containing the predicted interactions that will be evaluated  
# -m : directory containing trained models
# -r : Directory to save the evaluation reports
# -f : Directory to save the confusion matrices and plots 

# --------------------------------------------------------------------------

parser = OptionParser()
parser.add_option("-i", dest="iF",help="Directory with predicted interactions data sets", metavar="PATH")
parser.add_option("-r", dest="rF",help="File with reference interactions", metavar="PATH")
parser.add_option("-e", dest="eF",help="Directory to save evaluation reports",metavar="PATH")
parser.add_option("-f", dest="oF",help="Directory to store all the visual outputs",metavar="PATH")

(options, args) = parser.parse_args()
if len(args) > 0:
	parser.error("None parameters indicated.")
	sys.exit(1)

intDir = options.iF
rep = options.eF
refFile = options.rF
figDir = options.oF

# --------------------------- Functions ------------------------------------
# --------------------------------------------------------------------------

def class_values(class_):
	if  class_ == 'V' or class_ == "True":
		return 1
	elif class_ == 'F' or class_ == "False":
		return 0
	else:
		print('Error: class value not permitted')

def flatten(l):
	flat_list = []
	for sublist in l:
		for item in sublist:
			flat_list.append(item)

	l_arr = np.array(flat_list)
	l_uniq = list(np.unique(l_arr))

	return l_uniq

def match_pred_ref(pred,ref):
	
	ref_list = list(ref['interaction'])
	pred_list = list(pred['interaction'])
	print("===> Original predictions: {}".format(len(pred_list)))

	nf_index = []
	for prd in pred_list:
		if prd not in ref_list:
			
			#print("Prediction {} not found in reference".format(prd))

			indices = [i for i, x in enumerate(pred_list) if x == prd]
			nf_index.append(indices)

	nf_index = flatten(nf_index)

	pred = pred.drop(nf_index, axis=0)
	
	print("===> Predictions matched with reference: {}".format(len(pred['interaction'])))

	return pred


def get_true_class(pred,ref):

	index_T = ref.index[ref['curated_class'] == 1].tolist()
	true_intr = list(ref['interaction'][index_T])

	index_F = ref.index[ref['curated_class'] == 0].tolist()
	false_intr = list(ref['interaction'][index_F])

	class_list = []

	for intr in pred['interaction']:
		if intr in true_intr:
			class_list.append(1)
		elif intr in false_intr:
			class_list.append(0)
		else:
			print("FATAL ERROR: {} interaction not found cuz' SHE STUPID".format(intr))
	
	for intr in true_intr:
		if intr in false_intr:
			line = "Interaction has ambiguous classification " + intr
			print(line)
	return class_list
	

def plot_confusion_matrix(cm, classes,file,
						  normalize=False,
						  title='Confusion matrix',
						  cmap=plt.cm.Blues):
	if normalize:
		cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
		print("Normalized confusion matrix")
	else:
		print('Confusion matrix, without normalization')
	plt.figure()
	plt.imshow(cm, interpolation='nearest', cmap=cmap)
	plt.title(title)
	plt.colorbar()
	tick_marks = np.arange(len(classes))
	plt.xticks(tick_marks, classes, rotation=45)
	plt.yticks(tick_marks, classes)
	fmt = '.2f' if normalize else 'd'
	thresh = cm.max() / 2.
	for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
		plt.text(j, i, format(cm[i, j], fmt),
				 horizontalalignment="center",
				 color="white" if cm[i, j] > thresh else "black")
	plt.tight_layout()
	plt.ylabel('True label')
	plt.xlabel('Predicted label')
	plt.savefig(file,dpi=300)
	plt.close()

def evaluate(y_true,y_pred,class_names,mod_name):

	#Compute performance metrics 

	print(classification_report(y_true, y_pred, target_names=class_names))
	allmetrics = re.search(r'total\s+(.*)\n',classification_report(y_true, y_pred, target_names=class_names))
	print(allmetrics)
	if(allmetrics):
		metrics=allmetrics.group(1)

	vals = re.findall(r'(\d+\.\d+)',metrics)
	
	# Plot confusion matrix
	
	cnf_matrix = confusion_matrix(y_true, y_pred)
	np.set_printoptions(precision=2)
	figF = figDir+"/cm-"+mod_name+".png"
	plot_confusion_matrix(cnf_matrix, classes=class_names,title=mod_name,normalize=False,file=figF)  
	metricsDF = pd.DataFrame({mod_name:vals},index=['precision','recall','fscore']).transpose()
	metricsDF['model'] = mod_name

	return(metricsDF)


# ------------------------------- Main -------------------------------------
# --------------------------------------------------------------------------


colsRef =['gene-pbt-ID','disease-pbt-ID']
cols =['gene_id','disease_id']

names = ['Negative','Positive']

# Prediction files

preds = [f for f in os.listdir(intDir) if re.match(r'(.*)-consensus\.txt', f)]

# Reference files 

ref = pd.read_csv(refFile, header=0, sep='\t',dtype={'pmid':str,'gene-pbt-ID-ID':str,'disease-pbt-ID':str})
ref['curated_class'] = [class_values(c) for c in ref['curated_class']]
ref['interaction'] = ref.apply(lambda x: '|'.join(x[colsRef]),axis=1)

# Classification evaluation

metDF = pd.DataFrame() 

for p in preds:

	ppath = intDir + '/' + p
	mname = re.sub(r'predictions-','',re.sub('\.txt','',p))

	print("=============================================")
	print("Evaluating predictions of {}".format(mname))
	print("=============================================")

	# Read predicition file

	pdat = pd.read_csv(ppath, header=0, sep='\t',dtype={'PMID':str,'interaction':str})
	#pdat['gene_id'] = [str(g).upper() for g in pdat['gene_id']]
	#pdat['disease_id'] = [str(d).upper() for d in pdat['disease_id']]
	#pdat['interaction'] = pdat.apply(lambda x: '|'.join(x[cols]),axis=1)

	# Retrieve curated class
	pdat_found = match_pred_ref(pdat,ref)
	
	curated_class = get_true_class(pdat_found,ref)
	pred_class = list(pdat_found['Predicted-class'])
	

	# Evaluate 

	evdat = evaluate(curated_class,pred_class,names,mname)
	print(evdat)
	metDF = pd.concat([metDF,evdat])

#out = rep+"evaluation-report-trp.txt"
out = rep+"evaluation-report-consensus.txt"
metDF.to_csv(path_or_buf = out,sep='\t',index=False)

print("Evaluation reports at {}".format(out))




















