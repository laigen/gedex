import re
import sys
from optparse import OptionParser


parser = OptionParser()
parser.add_option("-i", dest="inF",help="Input file", metavar="PATH")
parser.add_option("-o", dest="outF",help="Output file",metavar="PATH")

(options, args) = parser.parse_args()
if len(args) > 0:
    parser.error("Please indicate an input and output files")
    sys.exit(1)

infile = options.inF
outdir = options.outF

b = 0
sentnum = 1
with open(infile,encoding='utf-8') as abstracts:
	for line in abstracts:
		if b == 1:
			if re.match("^\d+\|t\|", line):
				pmid = re.search("(\d+)\|t\|", line).group(1)
				line = re.search("\d+\|t\|(.*)", line).group(1)
				sentnum = 1

			newfile = open(outdir + pmid + "." + str(sentnum) + ".txt", 'w',encoding='utf-8')
			
			newline = pmid + "." + str(sentnum) + '\t' + line.rstrip() + '\n'
			newfile.write(newline)
			
			newfile.close()
			sentnum = sentnum + 1
			b = 0
		if line.startswith("Sentence"):
			b = 1
		if re.match("^\d+\|t\|", line):
			b = 1