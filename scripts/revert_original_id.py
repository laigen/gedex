import re
import os
import glob
import pandas as pd
from optparse import OptionParser
from joblib import Parallel, delayed



parser = OptionParser()
parser.add_option("-i", dest="indir",help="Prediction directory", metavar="PATH")
parser.add_option("-t", dest="pubT",help="Pubtator synonyms file", metavar="PATH")

(options, args) = parser.parse_args()

if len(args) > 0:
	parser.error("Please indicate all necessary inputs")
	sys.exit(1)

pubtator_syn = options.pubT
input_dir = options.indir

########################################### Reading files ############################################
print('::: Reading files...')
pubtator_synonym = pd.read_csv(pubtator_syn, sep='\t', encoding='utf-8', quoting=3,
                               dtype={'PMID':str, 'synonyms ID':str, 'Pubtator ID':str, 'gene_id': str})

# Creating dataframe for joining
synonym_relation = pubtator_synonym.loc[pubtator_synonym['type']=='Gene', ['PMID', 'Pubtator ID', 'synonyms ID']].drop_duplicates()
synonym_relation.rename(columns={'PMID':'pmid', 'synonyms ID':'gene_id'}, inplace=True)
synonym_relation.set_index(['pmid', 'gene_id'], inplace=True)



prediction_files = glob.glob(os.path.join(input_dir, 'predictions-*.txt'))
############################################
print('::: Reverting to original format...\n')

for file in prediction_files:
	filename = file.split('/')[-1]
	outfile = filename
	outdir = re.sub("/predictions","/predictions/original-ids",input_dir)

	print('\tReverting {} to original gene IDs.'.format(filename))

	prediction_df = pd.read_csv(file, sep='\t', quoting=3, encoding='utf-8', dtype={'pmid':str, 'gene_id': str})
	prediction_df.set_index(['pmid', 'gene_id'], inplace=True)

	predictions_org = prediction_df.join(synonym_relation, on=prediction_df.index.name)
	predictions_org.reset_index(inplace=True)
	predictions_org['gene_id'] = predictions_org['Pubtator ID']
	predictions_org.drop('Pubtator ID', axis=1, inplace=True)
	print("\tWriting output to {}".format(os.path.join(outdir,outfile)))
	predictions_org.to_csv(os.path.join(outdir,outfile), sep='\t', encoding='utf-8', index=False)
	print('\tFinal reversion file is {}\n'.format(outfile))

print('::: Finished reverting ids.')
